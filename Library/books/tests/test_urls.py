from django.test import SimpleTestCase
from django.urls import reverse, resolve
from books.views import index, book


class TestUrls(SimpleTestCase):

    def test_index_url_is_resolves(self):
        print("Test case running on url pattern 'index'")
        url = reverse('index')
        # print(resolve(url))
        self.assertEquals(resolve(url).func, index)
        
    def test_book_url_is_resolves(self):
        print("Test case running on url pattern 'book'")
        url = reverse('book')
        # print(resolve(url))
        self.assertEquals(resolve(url).func, book)
