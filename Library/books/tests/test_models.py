from venv import create
from django.test import TestCase
from books.models import Book 


class TestModels(TestCase):

    def setUp(self):
        print('Creating Book Object')
        self.book1 = Book.objects.create(
            name = 'OnePiece',
            author = 'SRJ',
            price = 9999
        )

    def test_book_details(self):
        print('Test case running on model Book Object')
        # qs = Book.objects.all()
        # self.assertAlmostEqual(qs.count(),2)
        b1 = Book.objects.get(name = 'OnePiece')
        self.assertEqual(b1.author,'SRJ')