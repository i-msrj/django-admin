from django.test import TestCase, TestCase, Client
from django.urls import reverse
from books.models import Book


class TestViews(TestCase):
   def setUp(self):
       self.client = Client()
       self.index_url = reverse('index')
       self.book_url = reverse('book')

   def test_index(self):
       print("Test case running on indexview function")
       response = self.client.get(self.index_url)
       self.assertEquals(response.status_code, 200)
       self.assertTemplateUsed(response, 'books/index.html')

   def test_book(self):
       print("Test case running on bookview function")
       response = self.client.get(self.book_url)
       self.assertEquals(response.status_code, 200)
       self.assertTemplateUsed(response, 'books/book.html')

    