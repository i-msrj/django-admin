from django.shortcuts import render
from . models import Book

# Create your views here.

def index(request):
    b = Book.objects.all()
    res = {'book':b}
    return render(request, 'books/index.html',res)


def book(request):
    return render(request, 'books/book.html')
